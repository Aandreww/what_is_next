// const Schedule = require('../models/Schedule');
// const moment = require('moment');
// const errorHandler = require('../utilis/errorHandler');
//
// module.exports.create = async function (req, res) {
//     try {
//         const schedule = await new Schedule({
//             name: req.body.name,
//             date: req.body.date,
//             startDate: req.body.startDate,
//             endDate: req.body.endDate,
//             type: req.body.type,
//             user: req.user.id
//         }).save();
//         res.status(201).json(schedule);
//     } catch (e) {
//         errorHandler(res, e);
//     }
// };
//
// module.exports.getAll = async function (req, res) {
//     try {
//         const schedules = await Schedule.find({
//             user: req.user.id
//         });
//         res.status(200).json(schedules);
//     } catch (e) {
//         errorHandler(res, e);
//     }
// };
//
// module.exports.getByWeek = async function (req, res) {
//     try {
//         let startDate = moment(req.params.date).startOf('week');
//         let endDate = moment(req.params.date).startOf('week').add(1, 'weeks');
//         const schedules = await Schedule.find(
//             {
//                 user: req.user.id,
//                 date: {
//                     '$gte': new Date(startDate),
//                     '$lte': new Date(endDate)
//                 }
//             }
//         );
//         res.status(200).json(schedules);
//     } catch (e) {
//         errorHandler(res, e);
//     }
// };
//
// module.exports.getById = async function (req, res) {
//     try {
//         const schedule = await Schedule.findById(req.params.id);
//         res.status(200).json(schedule);
//     } catch (e) {
//         errorHandler(res, e);
//     }
// };
//
// module.exports.update = async function (req, res) {
//     try {
//         const schedule = await Schedule.findOneAndUpdate(
//             {_id: req.params.id},
//             {$set: req.body},
//             {new: true}
//         );
//         res.status(200).json(schedule);
//     } catch (e) {
//         errorHandler(res, e);
//     }
// };
//
// module.exports.remove = async function (req, res) {
//     try {
//         await Schedule.deleteOne({_id: req.params.id});
//         res.status(200).json({
//             message: 'Schedule was removed.'
//         })
//     } catch (e) {
//         errorHandler(res, e);
//     }
// };
