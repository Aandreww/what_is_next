const Task = require('../models/Task');
const errorHandler = require('../utilis/errorHandler');
const moment = require('moment');

module.exports.create = async function (req, res) {
    try {
        const lastOrder = await Task
            .findOne({user: req.user.id})
            .sort({date: -1});

        const maxOrder = lastOrder ? lastOrder.order : 0;

        const task = await new Task({
            name: req.body.name,
            description: req.body.description,
            dueDate: req.body.dueDate,
            startDate: req.body.startDate,
            order: maxOrder + 1,
            user: req.user.id
        }).save();
        res.status(201).json(task);
    } catch (e) {
        errorHandler(res, e);
    }
};

module.exports.getAll = async function (req, res) {
    const query = {
        user: req.user.id
    };

    if (req.query.start) {
        query.date = {
            $gte: req.query.start
        }
    }

    if (req.query.end) {
        if (!query.date) {
            query.date = {}
        }

        query.date['$lte'] = req.query.end;
    }

    if (req.query.order) {
        query.order = +req.query.order;
    }

    try {
        const tasks = await Task
            .find(query)
            .sort({order: -1})
            .skip(+req.query.offset)
            .limit(+req.query.limit);
        res.status(200).json(tasks);
    } catch (e) {
        errorHandler(res, e);
    }
};

module.exports.getByWeek = async function (req, res) {
    try {
        let startDate = moment(req.params.date).startOf('week');
        let endDate = moment(req.params.date).startOf('week').add(1, 'weeks');
        const tasks = await Task.find(
            {
                user: req.user.id,
                date: {
                    '$gte': new Date(startDate),
                    '$lte': new Date(endDate)
                }
            }
        );
        res.status(200).json(tasks);
    } catch (e) {
        errorHandler(res, e);
    }
};

module.exports.getByDay = async function (req, res) {
    try {
        date = new Date(req.params.date).getDay();
        let startDate = moment(new Date(req.params.date)).startOf('day');
        let endDate = moment(new Date(req.params.date)).startOf('day').add(1, 'days');
        const tasks = await Task.find(
            {
                user: req.user.id,
                $or: [{
                    startDate: {
                        '$gte': new Date(startDate),
                        '$lte': new Date(endDate)
                    }
                },
                    {
                        endDate: {
                            '$gte': new Date(startDate),
                            '$lte': new Date(endDate)
                        }
                    }
                ]
            }
        );
        res.status(200).json(tasks);
    } catch (e) {
        errorHandler(res, e);
    }
};

module.exports.getById = async function (req, res) {
    try {
        const task = await Task.findById(req.params.id);
        res.status(200).json(task);
    } catch (e) {
        errorHandler(res, e);
    }
};

module.exports.update = async function (req, res) {
    try {
        const task = await Task.findOneAndUpdate(
            {_id: req.params.id},
            {$set: req.body},
            {new: true}
        );
        res.status(200).json(task);
    } catch (e) {
        errorHandler(res, e);
    }
};

module.exports.remove = async function (req, res) {
    try {
        await Task.deleteOne({_id: req.params.id});
        res.status(200).json({
            message: 'Task was removed.'
        })
    } catch (e) {
        errorHandler(res, e);
    }
};
