import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";
import {Message, Task} from "../interfaces";

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private http: HttpClient) {
  }

  gatAll(): Observable<Task[]> {
    return this.http.get<Task[]>('/api/task');
  }

  create(task: Task): Observable<Task> {
    return this.http.post<Task>('/api/task', task);
  }

  // getById(id: string): Observable<Task> {
  //   return this.http.get<Task>(`/api/task/${id}`);
  // }

  getByDay(date: Date): Observable<Task[]> {
    return this.http.get<Task[]>(`/api/task/day/${date}`);
  }

  // getByWeek(date: Date): Observable<Task> {
  //   return this.http.get<Task>(`/api/task/week/${date}`);
  // }

  update(task: Task): Observable<Task> {
    return this.http.patch<Task>(`/api/task/${task._id}`, task);
  }

  delete(id: string): Observable<Message> {
    return this.http.delete<Message>(`/api/task/${id}`);
  }

}
