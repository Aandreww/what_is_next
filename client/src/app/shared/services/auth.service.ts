import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";
import {tap} from "rxjs/operators";
import * as decode from "jwt-decode";

import {User} from "../interfaces";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private token = null;

  constructor(private http: HttpClient) {
  }

  login(user: User): Observable<{ token: string }> {
    return this.http.post<{ token: string }>('/api/auth/login', user)
      .pipe(
        tap(({token}) => {
          localStorage.setItem('token', token);
          this.setToken(token);
        })
      );
  }

  registration(user: User): Observable<User> {
    return this.http.post<User>('/api/auth/registration', user);
  }

  setToken(token: string) {
    this.token = token;
  }

  getToken(): string {
    return this.token;
  }

  isAuthenticated(): boolean {
    let current_time = Date.now() / 1000;
    let tokenInfo;
    if (this.token) {
      tokenInfo = decode(this.token);
    }
    return this.token && tokenInfo && tokenInfo.exp && tokenInfo.exp > current_time;
  }

  logout() {
    this.setToken(null);
    localStorage.removeItem('token');
  }

}
