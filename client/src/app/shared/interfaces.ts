export interface User {
  email: string;
  password: string;
}

export interface Task {
  _id?: string;
  name: string;
  description?: string;
  order?: number;
  date?: Date;
  startDate?: Date;
  dueDate?: Date;
  complete?: boolean;
  user?: string;
  opened?: boolean;
  deleting?: boolean;
}

export interface Message {
  message: string;
}

export interface Day {
  date: string;
  name: string;
  type: string;
  tasks?: Task[];
  dayPositions?: DayPosition[];
}

export interface DayPosition {
  timeStart: string;
  timeEnd: string;
  description: string;
  degree: number;
}
