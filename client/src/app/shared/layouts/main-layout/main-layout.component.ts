import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";
import {MatSidenav} from "@angular/material";

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.css']
})
export class MainLayoutComponent implements OnInit {

  links = [
    {url: '/today', name: 'Today'},
    {url: '/tasks', name: 'Tasks'},
    {url: '/schedule', name: 'Schedule'}
  ];

  @ViewChild('sidenav') sidenav: MatSidenav;

  screenWidth: number;

  constructor(private auth: AuthService,
              private router: Router) {
  }

  ngOnInit() {
    this.screenWidth = window.innerWidth;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenWidth = window.innerWidth;
  }

  closeBar() {
    if (this.screenWidth <= 992) {
      this.sidenav.close();
    }
  }

  logout(event: Event) {
    event.preventDefault();
    this.auth.logout();
    this.router.navigate(['/login'])
  }

}
