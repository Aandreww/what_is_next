import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-loader',
  template: `
    <div class="next-loader">
      <mat-spinner></mat-spinner>
    </div>`
})
export class LoaderComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
