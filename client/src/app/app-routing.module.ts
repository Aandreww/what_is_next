import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {LoginPageComponent} from "./login-page/login-page.component";
import {AuthLayoutComponent} from "./shared/layouts/auth-layout/auth-layout.component";
import {RegistrationPageComponent} from "./registration-page/registration-page.component";
import {MainLayoutComponent} from "./shared/layouts/main-layout/main-layout.component";
import {AuthGuard} from "./shared/classes/auth.guard";
import {TasksPageComponent} from "./tasks-page/tasks-page.component";
import {TodayPageComponent} from "./today-page/today-page.component";
import {SchedulePageComponent} from "./schedule-page/schedule-page.component";
import {AddTaskPageComponent} from "./tasks-page/add-task-page/add-task-page.component";

const routes: Routes = [
  {
    path: '', component: AuthLayoutComponent, children: [
      {path: '', redirectTo: '/login', pathMatch: 'full'},
      {path: 'login', component: LoginPageComponent},
      {path: 'registration', component: RegistrationPageComponent}
    ],
  },
  {
    path: '', component: MainLayoutComponent, canActivate: [AuthGuard], children: [
      {path: 'tasks', component: TasksPageComponent},
      {path: 'tasks/new', component: AddTaskPageComponent},
      {path: 'tasks/:id', component: TasksPageComponent},
      {path: 'today', component: TodayPageComponent},
      {path: 'schedule', component: SchedulePageComponent}
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {

}
