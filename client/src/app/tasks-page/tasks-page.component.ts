import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs/internal/Observable";
import {Task} from '../shared/interfaces';
import {TaskService} from "../shared/services/task.service";
import {switchMap, tap} from "rxjs/operators";
import {MatSnackBar} from "@angular/material";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Params} from "@angular/router";
import {of} from "rxjs";

@Component({
  selector: 'app-tasks-page',
  templateUrl: './tasks-page.component.html',
  styleUrls: ['./tasks-page.component.css']
})
export class TasksPageComponent implements OnInit {
  opened: boolean = false;

  tasks$: Observable<Task[]>;

  screenWidth: number;

  createNewTaskTamplate: boolean = false;

  form: FormGroup;

  tasksType: string = "All";

  constructor(private taskService: TaskService,
              public snackBar: MatSnackBar,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.getAllTasks();
    this.route.params
      .pipe(
        switchMap(
          (params: Params) => {
            if (params['id']) {
              this.tasks$ = this.tasks$.pipe(
                tap((tasks) => {
                  let searchedTask = tasks.find(task => task._id === params['id']);
                  this.tasksType = searchedTask.complete ? "Completed" : "All";
                  searchedTask.opened = true;
                })
              );
            }
            return of(null);
          }
        )
      )
      .subscribe(() => {
        },
        error => {
        });
  }

  public filterTasks(tasks: Task[]): Task[] {
    return tasks.filter(task => !task.complete && !task.deleting);
  }

  public completedTasks(tasks: Task[]): Task[] {
    return tasks.filter(task => task.complete && !task.deleting);
  }

  checkedTask(task: Task): void {
    let trigger = true;
    let snackBarRef = this.snackBar.open('Task completed.', 'Return', {duration: 3000});
    task.complete = true;
    snackBarRef.onAction().subscribe(() => {
      trigger = false;
      task.complete = false;
    });
    snackBarRef.afterDismissed().subscribe(() => {
      if (trigger) {
        this.taskService.update(task).subscribe(task => {
          console.log('Updated task: ', task);
        });
      }
    });
  }

  uncheckedTask(task: Task): void {
    let trigger = true;
    let snackBarRef = this.snackBar.open('Task unchecked.', 'Return', {duration: 3000});
    task.complete = false;
    snackBarRef.onAction().subscribe(() => {
      trigger = false;
      task.complete = true;
    });
    snackBarRef.afterDismissed().subscribe(() => {
      if (trigger) {
        this.taskService.update(task).subscribe(task => {
          console.log('Updated task: ', task);
        });
      }
    });
  }

  deleteTask(task: Task): void {
    let trigger = true;
    let snackBarRef = this.snackBar.open('Task deleted.', 'Return', {duration: 3000});
    task.deleting = true;
    this.taskService.delete(task._id).subscribe(task => {
      console.log('Deleted task: ', task);
    });
    snackBarRef.onAction().subscribe(() => {
      trigger = false;
      task.deleting = false;
    });
    snackBarRef.afterDismissed().subscribe(() => {
      if (trigger) {
        this.tasks$ = this.tasks$
          .pipe(
            tap(tasks => {
              let taskIndex = tasks.indexOf(searchedTask => searchedTask._id === task._id);
              return taskIndex !== -1 ? tasks.splice(taskIndex,1) : tasks;
            })
          );
        this.taskService.delete(task._id).subscribe(task => {
          console.log('Deleted task: ', task);
        });
      }
    });
  }

  public createNewTask(): void {
    this.createNewTaskTamplate = true;

    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      description: new FormControl('', []),
      date: new FormControl('', [])
    });
  }

  public onCreateNewTask(): void {
    this.form.disable();

    let newTask: Task = {
      name: this.form.get('name').value,
      description: this.form.get('description').value,
      startDate: this.form.get('date').value ? this.form.get('date').value[0] ? this.form.get('date').value[0] : this.form.get('date').value[1] :  new Date(),
      dueDate: this.form.get('date').value ? this.form.get('date').value[1] ? this.form.get('date').value[1] : this.form.get('date').value[0] :  new Date()
    };

    this.taskService.create(newTask)
      .subscribe(task => {
        console.log('Created task: ', task);
        this.getAllTasks();
      });
    this.createNewTaskTamplate = false;
  }

  private getAllTasks(): void {
    this.tasks$ = this.taskService.gatAll()
      .pipe(
        tap((tasks) => {
          tasks.forEach(tasks => {
            tasks.opened = false;
            tasks.deleting = false;
          })
        })
      );
    this.screenWidth = window.innerWidth;
  }

}
