import {Component, Input, OnInit} from '@angular/core';
import {Task} from "../../shared/interfaces";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {TaskService} from "../../shared/services/task.service";

@Component({
  selector: 'app-add-task-page',
  templateUrl: './add-task-page.component.html',
  styleUrls: ['./add-task-page.component.css']
})
export class AddTaskPageComponent implements OnInit {

  @Input()
  public task: Task;

  form: FormGroup;

  constructor(private taskService: TaskService) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      taskName: new FormControl(this.task.name, [Validators.required]),
      description: new FormControl(this.task.description, []),
      date: new FormControl([this.task.startDate,this.task.dueDate], [])
    });
  }

  public onSubmit(): void {
    this.form.disable();
    this.task.name = this.form.get('taskName').value;
    this.task.description = this.form.get('description').value;
    this.task.startDate = this.form.get('date').value[0];
    this.task.dueDate = this.form.get('date').value[1];
    this.taskService.update(this.task)
      .subscribe(task => console.log('Saved task: ', task));
    this.task.opened = !this.task.opened;
  }

}
