import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {AppComponent} from './app.component';
import {LoginPageComponent} from './login-page/login-page.component';
import {AppRoutingModule} from "./app-routing.module";
import {AuthLayoutComponent} from './shared/layouts/auth-layout/auth-layout.component';
import {MainLayoutComponent} from './shared/layouts/main-layout/main-layout.component';
import {RegistrationPageComponent} from './registration-page/registration-page.component';
import {TokenInterceptor} from "./shared/classes/token.interceptor";
import {TasksPageComponent} from './tasks-page/tasks-page.component';
import {TodayPageComponent} from './today-page/today-page.component';
import {SchedulePageComponent} from './schedule-page/schedule-page.component';
import {LoaderComponent} from './shared/components/loader/loader.component';
import {AddTaskPageComponent} from './tasks-page/add-task-page/add-task-page.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { DemoUtilsModule } from './shared/demo-utils/module';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatProgressSpinnerModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatStepperModule,
  MatToolbarModule
} from "@angular/material";
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from "ng-pick-datetime";
import {OwlFormFieldModule, OwlInputModule} from "owl-ng";

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    AuthLayoutComponent,
    MainLayoutComponent,
    RegistrationPageComponent,
    TasksPageComponent,
    TodayPageComponent,
    SchedulePageComponent,
    LoaderComponent,
    AddTaskPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatStepperModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    CalendarModule,
    DemoUtilsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    OwlInputModule,
    OwlFormFieldModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
