import {Component, ChangeDetectionStrategy, OnInit} from '@angular/core';
import {CalendarEvent, CalendarView} from 'angular-calendar';
import {TaskService} from "../shared/services/task.service";
import {addHours} from 'date-fns';
import {Observable} from "rxjs";
import {Task} from "../shared/interfaces";
import {tap} from "rxjs/operators";
import {Router} from "@angular/router";

@Component({
  selector: 'app-today-page',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './today-page.component.html'
})
export class TodayPageComponent implements OnInit {

  constructor(private taskService: TaskService,
              private router: Router) {
  }

  viewDate: Date = new Date();

  view: CalendarView = CalendarView.Day;

  tasks$: Observable<Task[]>;

  events: CalendarEvent[] = [];

  ngOnInit(): void {
    this.getTasks();
  }

  eventClicked({event}: { event: CalendarEvent }): void {
    this.router.navigate(['/tasks/' + event.id]);
  }

  private getTasks(): void {
    this.tasks$ = this.taskService.getByDay(this.viewDate).pipe(
      tap(tasks => {
        tasks.forEach(task => {
          this.events.push({
            id: task._id,
            start: addHours(new Date(task.startDate), 0),
            end: addHours(new Date(task.dueDate), 0),
            title: task.name
          });
        });
      })
    )
  }
}
