import {Component, OnInit} from '@angular/core';
import {Task} from "../shared/interfaces";
import {Observable, Subject} from "rxjs";
import {CalendarEventTimesChangedEvent, CalendarView} from "angular-calendar";
import {CalendarEvent} from "calendar-utils";
import {addHours, isSameDay, isSameMonth} from "date-fns";
import {colors} from "../shared/demo-utils/colors";
import {TaskService} from "../shared/services/task.service";
import {Router} from "@angular/router";
import {tap} from "rxjs/operators";

@Component({
  selector: 'app-schedule-page',
  templateUrl: './schedule-page.component.html',
  styleUrls: ['./schedule-page.component.css']
})
export class SchedulePageComponent implements OnInit {

  constructor(private taskService: TaskService,
              private router: Router) {
  }

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  refresh: Subject<any> = new Subject();

  activeDayIsOpen: boolean = true;

  viewDate: Date = new Date();

  tasks$: Observable<Task[]>;

  events: CalendarEvent[] = [];

  ngOnInit(): void {
    this.getTasks();
  }

  private getTasks(): void {
    this.tasks$ = this.taskService.gatAll().pipe(
      tap(tasks => {
        tasks.forEach(task => this.fillShedule(task));
      })
    )
  }

  eventClicked({event}: { event: CalendarEvent }): void {
    console.log('event: ', event);
    this.router.navigate(['/tasks/' + event.id]);
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      this.activeDayIsOpen = !((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0);
    }
  }

  eventTimesChanged({
                      event,
                      newStart,
                      newEnd
                    }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.tasks$ = this.tasks$.pipe(tap(tasks => {
      let newTask = tasks.find(task => task._id === event.id);
      newTask.startDate = newStart;
      newTask.dueDate = newEnd;
      this.taskService.update(newTask)
        .subscribe(task => console.log('Saved task: ', task));
      this.events = [];
      tasks.forEach(task => this.fillShedule(task));
      this.refresh.next();
    }));
  }

  private fillShedule(task: Task): void {
    let taskDuration = this.daysBetween(new Date(task.startDate),new Date(task.dueDate));
    this.events.push({
      id: task._id,
      start: addHours(new Date(task.startDate), 0),
      end: addHours(new Date(task.dueDate), 0),
      title: task.name,
      color: taskDuration < 1 ? colors.yellow : taskDuration < 2 ? colors.red : colors.blue,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    });
  }

  private daysBetween(date1: Date, date2: Date) {
    let one_day=1000*60*60*24;
    let date1_ms = date1.getTime();
    let date2_ms = date2.getTime();
    let difference_ms = date2_ms - date1_ms;
    return Math.round(difference_ms/one_day);
  }
}
