(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_page_login_page_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login-page/login-page.component */ "./src/app/login-page/login-page.component.ts");
/* harmony import */ var _shared_layouts_auth_layout_auth_layout_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shared/layouts/auth-layout/auth-layout.component */ "./src/app/shared/layouts/auth-layout/auth-layout.component.ts");
/* harmony import */ var _registration_page_registration_page_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./registration-page/registration-page.component */ "./src/app/registration-page/registration-page.component.ts");
/* harmony import */ var _shared_layouts_main_layout_main_layout_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shared/layouts/main-layout/main-layout.component */ "./src/app/shared/layouts/main-layout/main-layout.component.ts");
/* harmony import */ var _shared_classes_auth_guard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./shared/classes/auth.guard */ "./src/app/shared/classes/auth.guard.ts");
/* harmony import */ var _tasks_page_tasks_page_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./tasks-page/tasks-page.component */ "./src/app/tasks-page/tasks-page.component.ts");
/* harmony import */ var _today_page_today_page_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./today-page/today-page.component */ "./src/app/today-page/today-page.component.ts");
/* harmony import */ var _schedule_page_schedule_page_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./schedule-page/schedule-page.component */ "./src/app/schedule-page/schedule-page.component.ts");
/* harmony import */ var _tasks_page_add_task_page_add_task_page_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./tasks-page/add-task-page/add-task-page.component */ "./src/app/tasks-page/add-task-page/add-task-page.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var routes = [
    {
        path: '', component: _shared_layouts_auth_layout_auth_layout_component__WEBPACK_IMPORTED_MODULE_3__["AuthLayoutComponent"], children: [
            { path: '', redirectTo: '/login', pathMatch: 'full' },
            { path: 'login', component: _login_page_login_page_component__WEBPACK_IMPORTED_MODULE_2__["LoginPageComponent"] },
            { path: 'registration', component: _registration_page_registration_page_component__WEBPACK_IMPORTED_MODULE_4__["RegistrationPageComponent"] }
        ],
    },
    {
        path: '', component: _shared_layouts_main_layout_main_layout_component__WEBPACK_IMPORTED_MODULE_5__["MainLayoutComponent"], canActivate: [_shared_classes_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]], children: [
            { path: 'tasks', component: _tasks_page_tasks_page_component__WEBPACK_IMPORTED_MODULE_7__["TasksPageComponent"] },
            { path: 'tasks/new', component: _tasks_page_add_task_page_add_task_page_component__WEBPACK_IMPORTED_MODULE_10__["AddTaskPageComponent"] },
            { path: 'tasks/:id', component: _tasks_page_tasks_page_component__WEBPACK_IMPORTED_MODULE_7__["TasksPageComponent"] },
            { path: 'today', component: _today_page_today_page_component__WEBPACK_IMPORTED_MODULE_8__["TodayPageComponent"] },
            { path: 'schedule', component: _schedule_page_schedule_page_component__WEBPACK_IMPORTED_MODULE_9__["SchedulePageComponent"] }
        ]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)
            ],
            exports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]
            ]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(auth) {
        this.auth = auth;
    }
    AppComponent.prototype.ngOnInit = function () {
        var potentialToken = localStorage.getItem('token');
        if (potentialToken !== null) {
            this.auth.setToken(potentialToken);
        }
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: '<router-outlet></router-outlet>'
        }),
        __metadata("design:paramtypes", [_shared_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _login_page_login_page_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login-page/login-page.component */ "./src/app/login-page/login-page.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _shared_layouts_auth_layout_auth_layout_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./shared/layouts/auth-layout/auth-layout.component */ "./src/app/shared/layouts/auth-layout/auth-layout.component.ts");
/* harmony import */ var _shared_layouts_main_layout_main_layout_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./shared/layouts/main-layout/main-layout.component */ "./src/app/shared/layouts/main-layout/main-layout.component.ts");
/* harmony import */ var _registration_page_registration_page_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./registration-page/registration-page.component */ "./src/app/registration-page/registration-page.component.ts");
/* harmony import */ var _shared_classes_token_interceptor__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./shared/classes/token.interceptor */ "./src/app/shared/classes/token.interceptor.ts");
/* harmony import */ var _tasks_page_tasks_page_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./tasks-page/tasks-page.component */ "./src/app/tasks-page/tasks-page.component.ts");
/* harmony import */ var _today_page_today_page_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./today-page/today-page.component */ "./src/app/today-page/today-page.component.ts");
/* harmony import */ var _schedule_page_schedule_page_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./schedule-page/schedule-page.component */ "./src/app/schedule-page/schedule-page.component.ts");
/* harmony import */ var _shared_components_loader_loader_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./shared/components/loader/loader.component */ "./src/app/shared/components/loader/loader.component.ts");
/* harmony import */ var _tasks_page_add_task_page_add_task_page_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./tasks-page/add-task-page/add-task-page.component */ "./src/app/tasks-page/add-task-page/add-task-page.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _shared_demo_utils_module__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./shared/demo-utils/module */ "./src/app/shared/demo-utils/module.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var angular_calendar__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! angular-calendar */ "./node_modules/angular-calendar/fesm5/angular-calendar.js");
/* harmony import */ var angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! angular-calendar/date-adapters/date-fns */ "./node_modules/angular-calendar/date-adapters/date-fns/index.js");
/* harmony import */ var angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_20__);
/* harmony import */ var ng_pick_datetime__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ng-pick-datetime */ "./node_modules/ng-pick-datetime/picker.js");
/* harmony import */ var owl_ng__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! owl-ng */ "./node_modules/owl-ng/owl-ng.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _login_page_login_page_component__WEBPACK_IMPORTED_MODULE_5__["LoginPageComponent"],
                _shared_layouts_auth_layout_auth_layout_component__WEBPACK_IMPORTED_MODULE_7__["AuthLayoutComponent"],
                _shared_layouts_main_layout_main_layout_component__WEBPACK_IMPORTED_MODULE_8__["MainLayoutComponent"],
                _registration_page_registration_page_component__WEBPACK_IMPORTED_MODULE_9__["RegistrationPageComponent"],
                _tasks_page_tasks_page_component__WEBPACK_IMPORTED_MODULE_11__["TasksPageComponent"],
                _today_page_today_page_component__WEBPACK_IMPORTED_MODULE_12__["TodayPageComponent"],
                _schedule_page_schedule_page_component__WEBPACK_IMPORTED_MODULE_13__["SchedulePageComponent"],
                _shared_components_loader_loader_component__WEBPACK_IMPORTED_MODULE_14__["LoaderComponent"],
                _tasks_page_add_task_page_add_task_page_component__WEBPACK_IMPORTED_MODULE_15__["AddTaskPageComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_16__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatSnackBarModule"],
                angular_calendar__WEBPACK_IMPORTED_MODULE_19__["CalendarModule"].forRoot({
                    provide: angular_calendar__WEBPACK_IMPORTED_MODULE_19__["DateAdapter"],
                    useFactory: angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_20__["adapterFactory"]
                }),
                angular_calendar__WEBPACK_IMPORTED_MODULE_19__["CalendarModule"],
                _shared_demo_utils_module__WEBPACK_IMPORTED_MODULE_17__["DemoUtilsModule"],
                ng_pick_datetime__WEBPACK_IMPORTED_MODULE_21__["OwlDateTimeModule"],
                ng_pick_datetime__WEBPACK_IMPORTED_MODULE_21__["OwlNativeDateTimeModule"],
                owl_ng__WEBPACK_IMPORTED_MODULE_22__["OwlInputModule"],
                owl_ng__WEBPACK_IMPORTED_MODULE_22__["OwlFormFieldModule"]
            ],
            providers: [
                {
                    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HTTP_INTERCEPTORS"],
                    useClass: _shared_classes_token_interceptor__WEBPACK_IMPORTED_MODULE_10__["TokenInterceptor"],
                    multi: true
                }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/login-page/login-page.component.css":
/*!*****************************************************!*\
  !*** ./src/app/login-page/login-page.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/login-page/login-page.component.html":
/*!******************************************************!*\
  !*** ./src/app/login-page/login-page.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form\r\n  [formGroup]=\"form\"\r\n  (ngSubmit)=\"onSubmit()\">\r\n  <mat-card-title>\r\n    Login\r\n  </mat-card-title>\r\n  <mat-card-content>\r\n    <mat-form-field>\r\n      <input\r\n        formControlName=\"email\"\r\n        matInput\r\n        placeholder=\"Email\"\r\n        type=\"email\">\r\n      <mat-error *ngIf=\"form.get('email').invalid && form.get('email').touched && form.get('email').errors['required']\">\r\n        Email is <strong>required</strong>\r\n      </mat-error>\r\n      <mat-error *ngIf=\"form.get('email').invalid && form.get('email').touched && form.get('email').errors['email']\">\r\n        Please enter a valid email address\r\n      </mat-error>\r\n    </mat-form-field>\r\n    <br/>\r\n    <mat-form-field>\r\n      <input\r\n        formControlName=\"password\"\r\n        matInput\r\n        placeholder=\"Password\"\r\n        type=\"password\">\r\n      <mat-error\r\n        *ngIf=\"form.get('password').invalid\r\n        && form.get('password').touched\r\n        && form.get('password').errors['required']\">\r\n        Password is <strong>required</strong>\r\n      </mat-error>\r\n      <mat-error\r\n        *ngIf=\"form.get('password').invalid\r\n        && form.get('password').touched\r\n        && form.get('password').errors['minlength']\r\n        && form.get('password').errors['minlength']['requiredLength']\">\r\n        Password must be at least {{form.get('password').errors['minlength']['requiredLength']}} characters long.\r\n        Now {{form.get('password').errors['minlength']['actualLength']}}.\r\n      </mat-error>\r\n    </mat-form-field>\r\n  </mat-card-content>\r\n  <mat-card-actions>\r\n    <button\r\n      mat-flat-button\r\n      type=\"submit\"\r\n      color=\"primary\"\r\n      [disabled]=\"form.invalid || form.disabled\">\r\n      Log In\r\n    </button>\r\n    <button mat-flat-button routerLink=\"/registration\">Registration</button>\r\n  </mat-card-actions>\r\n</form>\r\n"

/***/ }),

/***/ "./src/app/login-page/login-page.component.ts":
/*!****************************************************!*\
  !*** ./src/app/login-page/login-page.component.ts ***!
  \****************************************************/
/*! exports provided: LoginPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageComponent", function() { return LoginPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/internal/Subject */ "./node_modules/rxjs/internal/Subject.js");
/* harmony import */ var rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginPageComponent = /** @class */ (function () {
    function LoginPageComponent(auth, router) {
        this.auth = auth;
        this.router = router;
        this.unsubscribe = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_4__["Subject"]();
    }
    LoginPageComponent.prototype.ngOnInit = function () {
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(5)])
        });
    };
    LoginPageComponent.prototype.ngOnDestroy = function () {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    };
    LoginPageComponent.prototype.onSubmit = function () {
        var _this = this;
        this.form.disable();
        this.auth.login(this.form.value)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["takeUntil"])(this.unsubscribe))
            .subscribe(function () {
            _this.router.navigate(['/today']);
        }, function (error) {
            _this.form.enable();
        });
    };
    LoginPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login-page',
            template: __webpack_require__(/*! ./login-page.component.html */ "./src/app/login-page/login-page.component.html"),
            styles: [__webpack_require__(/*! ./login-page.component.css */ "./src/app/login-page/login-page.component.css")]
        }),
        __metadata("design:paramtypes", [_shared_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], LoginPageComponent);
    return LoginPageComponent;
}());



/***/ }),

/***/ "./src/app/registration-page/registration-page.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/registration-page/registration-page.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/registration-page/registration-page.component.html":
/*!********************************************************************!*\
  !*** ./src/app/registration-page/registration-page.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form\r\n  [formGroup]=\"form\"\r\n  (ngSubmit)=\"onSubmit()\">\r\n  <mat-card-title>\r\n    Registration\r\n  </mat-card-title>\r\n  <mat-card-content>\r\n    <mat-form-field>\r\n      <input\r\n        formControlName=\"email\"\r\n        matInput\r\n        placeholder=\"Email\"\r\n        type=\"email\">\r\n      <mat-error *ngIf=\"form.get('email').invalid && form.get('email').touched && form.get('email').errors['required']\">\r\n        Email is <strong>required</strong>\r\n      </mat-error>\r\n      <mat-error *ngIf=\"form.get('email').invalid && form.get('email').touched && form.get('email').errors['email']\">\r\n        Please enter a valid email address\r\n      </mat-error>\r\n    </mat-form-field>\r\n    <br/>\r\n    <mat-form-field>\r\n      <input\r\n        formControlName=\"password\"\r\n        matInput\r\n        placeholder=\"Password\"\r\n        type=\"password\">\r\n      <mat-error\r\n        *ngIf=\"form.get('password').invalid\r\n        && form.get('password').touched\r\n        && form.get('password').errors['required']\">\r\n        Password is <strong>required</strong>\r\n      </mat-error>\r\n      <mat-error\r\n        *ngIf=\"form.get('password').invalid\r\n        && form.get('password').touched\r\n        && form.get('password').errors['minlength']\r\n        && form.get('password').errors['minlength']['requiredLength']\">\r\n        Password must be at least {{form.get('password').errors['minlength']['requiredLength']}} characters long.\r\n        Now {{form.get('password').errors['minlength']['actualLength']}}.\r\n      </mat-error>\r\n    </mat-form-field>\r\n    <br/>\r\n    <mat-form-field>\r\n      <input\r\n        formControlName=\"confirmPassword\"\r\n        matInput\r\n        [errorStateMatcher]=\"matcher\"\r\n        placeholder=\"Confirm Password\"\r\n        type=\"password\">\r\n      <mat-error\r\n        *ngIf=\"form.hasError('notSame')\">\r\n        Passwords must match\r\n      </mat-error>\r\n    </mat-form-field>\r\n  </mat-card-content>\r\n  <mat-card-actions>\r\n    <button\r\n      mat-flat-button\r\n      type=\"submit\"\r\n      color=\"primary\"\r\n      [disabled]=\"form.invalid || form.disabled\">\r\n      Registration\r\n    </button>\r\n    <button mat-flat-button routerLink=\"/login\">Log In</button>\r\n  </mat-card-actions>\r\n</form>\r\n"

/***/ }),

/***/ "./src/app/registration-page/registration-page.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/registration-page/registration-page.component.ts ***!
  \******************************************************************/
/*! exports provided: MyErrorStateMatcher, RegistrationPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyErrorStateMatcher", function() { return MyErrorStateMatcher; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationPageComponent", function() { return RegistrationPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");
/* harmony import */ var rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/internal/Subject */ "./node_modules/rxjs/internal/Subject.js");
/* harmony import */ var rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MyErrorStateMatcher = /** @class */ (function () {
    function MyErrorStateMatcher() {
    }
    MyErrorStateMatcher.prototype.isErrorState = function (control, form) {
        var invalidCtrl = !!(control && control.invalid && control.parent.dirty);
        var invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);
        return control.touched && (invalidCtrl || invalidParent);
    };
    return MyErrorStateMatcher;
}());

var RegistrationPageComponent = /** @class */ (function () {
    function RegistrationPageComponent(auth, router) {
        this.auth = auth;
        this.router = router;
        this.matcher = new MyErrorStateMatcher();
        this.unsubscribe = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_4__["Subject"]();
    }
    RegistrationPageComponent.prototype.ngOnInit = function () {
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(5)]),
            confirmPassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
        }, this.checkPasswords);
    };
    RegistrationPageComponent.prototype.ngOnDestroy = function () {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    };
    RegistrationPageComponent.prototype.checkPasswords = function (group) {
        var pass = group.controls.password.value;
        var confirmPass = group.controls.confirmPassword.value;
        return pass === confirmPass ? null : { notSame: true };
    };
    RegistrationPageComponent.prototype.onSubmit = function () {
        var _this = this;
        this.form.disable();
        this.auth.registration(this.form.value)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["takeUntil"])(this.unsubscribe))
            .subscribe(function () {
            _this.router.navigate(['/login'], {
                queryParams: {
                    registered: true
                }
            });
        }, function (error) {
            _this.form.enable();
        });
    };
    RegistrationPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-registration-page',
            template: __webpack_require__(/*! ./registration-page.component.html */ "./src/app/registration-page/registration-page.component.html"),
            styles: [__webpack_require__(/*! ./registration-page.component.css */ "./src/app/registration-page/registration-page.component.css")]
        }),
        __metadata("design:paramtypes", [_shared_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], RegistrationPageComponent);
    return RegistrationPageComponent;
}());



/***/ }),

/***/ "./src/app/schedule-page/schedule-page.component.css":
/*!***********************************************************!*\
  !*** ./src/app/schedule-page/schedule-page.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/schedule-page/schedule-page.component.html":
/*!************************************************************!*\
  !*** ./src/app/schedule-page/schedule-page.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row text-center\">\r\n  <div class=\"col-md-4\">\r\n    <div class=\"btn-group\">\r\n      <button class=\"next-button\"\r\n              mat-raised-button\r\n              color=\"primary\"\r\n              mwlCalendarPreviousView\r\n              [view]=\"view\"\r\n              [(viewDate)]=\"viewDate\"\r\n              (viewDateChange)=\"activeDayIsOpen = false\">\r\n        Previous\r\n      </button>\r\n      <button class=\"next-button\"\r\n              mat-raised-button\r\n              color=\"primary\"\r\n              mwlCalendarToday\r\n              [(viewDate)]=\"viewDate\">\r\n        Today\r\n      </button>\r\n      <button class=\"next-button\"\r\n              mat-raised-button\r\n              color=\"primary\"\r\n              mwlCalendarNextView\r\n              [view]=\"view\"\r\n              [(viewDate)]=\"viewDate\"\r\n              (viewDateChange)=\"activeDayIsOpen = false\">\r\n        Next\r\n      </button>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-md-4\">\r\n    <h3 class=\"h3-title\">{{ viewDate | calendarDate:(view + 'ViewTitle'):'en' }}</h3>\r\n  </div>\r\n  <div class=\"col-md-4\">\r\n    <div class=\"btn-group\">\r\n      <button class=\"next-button\"\r\n              mat-raised-button\r\n              color=\"primary\"\r\n              (click)=\"view = CalendarView.Month\"\r\n              [class.active]=\"view === CalendarView.Month\">\r\n        Month\r\n      </button>\r\n      <button class=\"next-button\"\r\n              mat-raised-button\r\n              color=\"primary\"\r\n              (click)=\"view = CalendarView.Week\"\r\n              [class.active]=\"view === CalendarView.Week\">\r\n        Week\r\n      </button>\r\n      <button class=\"next-button\"\r\n              mat-raised-button\r\n              color=\"primary\"\r\n              (click)=\"view = CalendarView.Day\"\r\n              [class.active]=\"view === CalendarView.Day\">\r\n        Day\r\n      </button>\r\n    </div>\r\n  </div>\r\n</div>\r\n<br>\r\n<div *ngIf=\"tasks$ | async; else loader\"\r\n          [ngSwitch]=\"view\">\r\n  <mwl-calendar-month-view\r\n    *ngSwitchCase=\"CalendarView.Month\"\r\n    [viewDate]=\"viewDate\"\r\n    [events]=\"events\"\r\n    [refresh]=\"refresh\"\r\n    [activeDayIsOpen]=\"activeDayIsOpen\"\r\n    (dayClicked)=\"dayClicked($event.day)\"\r\n    (eventClicked)=\"eventClicked($event)\"\r\n    (eventTimesChanged)=\"eventTimesChanged($event)\">\r\n  </mwl-calendar-month-view>\r\n  <mwl-calendar-week-view\r\n    *ngSwitchCase=\"CalendarView.Week\"\r\n    [viewDate]=\"viewDate\"\r\n    [events]=\"events\"\r\n    [refresh]=\"refresh\"\r\n    (eventClicked)=\"eventClicked($event)\"\r\n    (eventTimesChanged)=\"eventTimesChanged($event)\">\r\n  </mwl-calendar-week-view>\r\n  <mwl-calendar-day-view\r\n    *ngSwitchCase=\"CalendarView.Day\"\r\n    [viewDate]=\"viewDate\"\r\n    [events]=\"events\"\r\n    [refresh]=\"refresh\"\r\n    (eventClicked)=\"eventClicked($event)\"\r\n    (eventTimesChanged)=\"eventTimesChanged($event)\">\r\n  </mwl-calendar-day-view>\r\n</div>\r\n\r\n<ng-template #loader>\r\n  <app-loader></app-loader>\r\n</ng-template>\r\n"

/***/ }),

/***/ "./src/app/schedule-page/schedule-page.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/schedule-page/schedule-page.component.ts ***!
  \**********************************************************/
/*! exports provided: SchedulePageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchedulePageComponent", function() { return SchedulePageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var angular_calendar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-calendar */ "./node_modules/angular-calendar/fesm5/angular-calendar.js");
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! date-fns */ "./node_modules/date-fns/index.js");
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(date_fns__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _shared_demo_utils_colors__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/demo-utils/colors */ "./src/app/shared/demo-utils/colors.ts");
/* harmony import */ var _shared_services_task_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared/services/task.service */ "./src/app/shared/services/task.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SchedulePageComponent = /** @class */ (function () {
    function SchedulePageComponent(taskService, router) {
        this.taskService = taskService;
        this.router = router;
        this.view = angular_calendar__WEBPACK_IMPORTED_MODULE_2__["CalendarView"].Month;
        this.CalendarView = angular_calendar__WEBPACK_IMPORTED_MODULE_2__["CalendarView"];
        this.refresh = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.activeDayIsOpen = true;
        this.viewDate = new Date();
        this.events = [];
    }
    SchedulePageComponent.prototype.ngOnInit = function () {
        this.getTasks();
    };
    SchedulePageComponent.prototype.getTasks = function () {
        var _this = this;
        this.tasks$ = this.taskService.gatAll().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["tap"])(function (tasks) {
            tasks.forEach(function (task) { return _this.fillShedule(task); });
        }));
    };
    SchedulePageComponent.prototype.eventClicked = function (_a) {
        var event = _a.event;
        console.log('event: ', event);
        this.router.navigate(['/tasks/' + event.id]);
    };
    SchedulePageComponent.prototype.dayClicked = function (_a) {
        var date = _a.date, events = _a.events;
        if (Object(date_fns__WEBPACK_IMPORTED_MODULE_3__["isSameMonth"])(date, this.viewDate)) {
            this.viewDate = date;
            this.activeDayIsOpen = !((Object(date_fns__WEBPACK_IMPORTED_MODULE_3__["isSameDay"])(this.viewDate, date) && this.activeDayIsOpen === true) ||
                events.length === 0);
        }
    };
    SchedulePageComponent.prototype.eventTimesChanged = function (_a) {
        var _this = this;
        var event = _a.event, newStart = _a.newStart, newEnd = _a.newEnd;
        event.start = newStart;
        event.end = newEnd;
        this.tasks$ = this.tasks$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["tap"])(function (tasks) {
            var newTask = tasks.find(function (task) { return task._id === event.id; });
            newTask.startDate = newStart;
            newTask.dueDate = newEnd;
            _this.taskService.update(newTask)
                .subscribe(function (task) { return console.log('Saved task: ', task); });
            _this.events = [];
            tasks.forEach(function (task) { return _this.fillShedule(task); });
            _this.refresh.next();
        }));
    };
    SchedulePageComponent.prototype.fillShedule = function (task) {
        var taskDuration = this.daysBetween(new Date(task.startDate), new Date(task.dueDate));
        this.events.push({
            id: task._id,
            start: Object(date_fns__WEBPACK_IMPORTED_MODULE_3__["addHours"])(new Date(task.startDate), 0),
            end: Object(date_fns__WEBPACK_IMPORTED_MODULE_3__["addHours"])(new Date(task.dueDate), 0),
            title: task.name,
            color: taskDuration < 1 ? _shared_demo_utils_colors__WEBPACK_IMPORTED_MODULE_4__["colors"].yellow : taskDuration < 2 ? _shared_demo_utils_colors__WEBPACK_IMPORTED_MODULE_4__["colors"].red : _shared_demo_utils_colors__WEBPACK_IMPORTED_MODULE_4__["colors"].blue,
            resizable: {
                beforeStart: true,
                afterEnd: true
            },
            draggable: true
        });
    };
    SchedulePageComponent.prototype.daysBetween = function (date1, date2) {
        var one_day = 1000 * 60 * 60 * 24;
        var date1_ms = date1.getTime();
        var date2_ms = date2.getTime();
        var difference_ms = date2_ms - date1_ms;
        return Math.round(difference_ms / one_day);
    };
    SchedulePageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-schedule-page',
            template: __webpack_require__(/*! ./schedule-page.component.html */ "./src/app/schedule-page/schedule-page.component.html"),
            styles: [__webpack_require__(/*! ./schedule-page.component.css */ "./src/app/schedule-page/schedule-page.component.css")]
        }),
        __metadata("design:paramtypes", [_shared_services_task_service__WEBPACK_IMPORTED_MODULE_5__["TaskService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], SchedulePageComponent);
    return SchedulePageComponent;
}());



/***/ }),

/***/ "./src/app/shared/classes/auth.guard.ts":
/*!**********************************************!*\
  !*** ./src/app/shared/classes/auth.guard.ts ***!
  \**********************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/shared/services/auth.service.ts");
/* harmony import */ var rxjs_internal_observable_of__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/internal/observable/of */ "./node_modules/rxjs/internal/observable/of.js");
/* harmony import */ var rxjs_internal_observable_of__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_observable_of__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthGuard = /** @class */ (function () {
    function AuthGuard(auth, router) {
        this.auth = auth;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        if (this.auth.isAuthenticated()) {
            return Object(rxjs_internal_observable_of__WEBPACK_IMPORTED_MODULE_3__["of"])(true);
        }
        else {
            this.router.navigate(['/login'], {
                queryParams: {
                    accessDenied: true
                }
            });
            return Object(rxjs_internal_observable_of__WEBPACK_IMPORTED_MODULE_3__["of"])(false);
        }
    };
    AuthGuard.prototype.canActivateChild = function (childRoute, state) {
        return this.canActivate(childRoute, state);
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/shared/classes/token.interceptor.ts":
/*!*****************************************************!*\
  !*** ./src/app/shared/classes/token.interceptor.ts ***!
  \*****************************************************/
/*! exports provided: TokenInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenInterceptor", function() { return TokenInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/shared/services/auth.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs_internal_observable_throwError__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/internal/observable/throwError */ "./node_modules/rxjs/internal/observable/throwError.js");
/* harmony import */ var rxjs_internal_observable_throwError__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_observable_throwError__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TokenInterceptor = /** @class */ (function () {
    function TokenInterceptor(auth, router) {
        this.auth = auth;
        this.router = router;
    }
    TokenInterceptor.prototype.intercept = function (req, next) {
        var _this = this;
        if (this.auth.isAuthenticated()) {
            req = req.clone({
                setHeaders: {
                    Authorization: this.auth.getToken()
                }
            });
        }
        return next.handle(req).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(function (error) {
            return _this.handleAuthError(error);
        }));
    };
    TokenInterceptor.prototype.handleAuthError = function (error) {
        if (error.status === 401) {
            this.router.navigate(['/login'], {
                queryParams: {
                    sessionFailed: true
                }
            });
        }
        return Object(rxjs_internal_observable_throwError__WEBPACK_IMPORTED_MODULE_3__["throwError"])(error);
    };
    TokenInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], TokenInterceptor);
    return TokenInterceptor;
}());



/***/ }),

/***/ "./src/app/shared/components/loader/loader.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/shared/components/loader/loader.component.ts ***!
  \**************************************************************/
/*! exports provided: LoaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderComponent", function() { return LoaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LoaderComponent = /** @class */ (function () {
    function LoaderComponent() {
    }
    LoaderComponent.prototype.ngOnInit = function () {
    };
    LoaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-loader',
            template: "\n    <div class=\"next-loader\">\n      <mat-spinner></mat-spinner>\n    </div>"
        }),
        __metadata("design:paramtypes", [])
    ], LoaderComponent);
    return LoaderComponent;
}());



/***/ }),

/***/ "./src/app/shared/demo-utils/calendar-header.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/shared/demo-utils/calendar-header.component.ts ***!
  \****************************************************************/
/*! exports provided: CalendarHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CalendarHeaderComponent", function() { return CalendarHeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CalendarHeaderComponent = /** @class */ (function () {
    function CalendarHeaderComponent() {
        this.locale = 'en';
        this.viewChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.viewDateChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], CalendarHeaderComponent.prototype, "view", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Date)
    ], CalendarHeaderComponent.prototype, "viewDate", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], CalendarHeaderComponent.prototype, "locale", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], CalendarHeaderComponent.prototype, "viewChange", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], CalendarHeaderComponent.prototype, "viewDateChange", void 0);
    CalendarHeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'mwl-demo-utils-calendar-header',
            template: "\n    <div class=\"row text-center\">\n      <div class=\"col-md-4\">\n        <div class=\"btn-group\">\n          <div\n            class=\"btn btn-primary\"\n            mwlCalendarPreviousView\n            [view]=\"view\"\n            [(viewDate)]=\"viewDate\"\n            (viewDateChange)=\"viewDateChange.next(viewDate)\"\n          >\n            Previous\n          </div>\n          <div\n            class=\"btn btn-outline-secondary\"\n            mwlCalendarToday\n            [(viewDate)]=\"viewDate\"\n            (viewDateChange)=\"viewDateChange.next(viewDate)\"\n          >\n            Today\n          </div>\n          <div\n            class=\"btn btn-primary\"\n            mwlCalendarNextView\n            [view]=\"view\"\n            [(viewDate)]=\"viewDate\"\n            (viewDateChange)=\"viewDateChange.next(viewDate)\"\n          >\n            Next\n          </div>\n        </div>\n      </div>\n      <div class=\"col-md-4\">\n        <h3>{{ viewDate | calendarDate: view + 'ViewTitle':locale }}</h3>\n      </div>\n      <div class=\"col-md-4\">\n        <div class=\"btn-group\">\n          <div\n            class=\"btn btn-primary\"\n            (click)=\"viewChange.emit('month')\"\n            [class.active]=\"view === 'month'\"\n          >\n            Month\n          </div>\n          <div\n            class=\"btn btn-primary\"\n            (click)=\"viewChange.emit('week')\"\n            [class.active]=\"view === 'week'\"\n          >\n            Week\n          </div>\n          <div\n            class=\"btn btn-primary\"\n            (click)=\"viewChange.emit('day')\"\n            [class.active]=\"view === 'day'\"\n          >\n            Day\n          </div>\n        </div>\n      </div>\n    </div>\n    <br />\n  "
        })
    ], CalendarHeaderComponent);
    return CalendarHeaderComponent;
}());



/***/ }),

/***/ "./src/app/shared/demo-utils/colors.ts":
/*!*********************************************!*\
  !*** ./src/app/shared/demo-utils/colors.ts ***!
  \*********************************************/
/*! exports provided: colors */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "colors", function() { return colors; });
var colors = {
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
    },
    blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF'
    },
    yellow: {
        primary: '#e3bc08',
        secondary: '#FDF1BA'
    }
};


/***/ }),

/***/ "./src/app/shared/demo-utils/module.ts":
/*!*********************************************!*\
  !*** ./src/app/shared/demo-utils/module.ts ***!
  \*********************************************/
/*! exports provided: DemoUtilsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DemoUtilsModule", function() { return DemoUtilsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_calendar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-calendar */ "./node_modules/angular-calendar/fesm5/angular-calendar.js");
/* harmony import */ var _calendar_header_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./calendar-header.component */ "./src/app/shared/demo-utils/calendar-header.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var DemoUtilsModule = /** @class */ (function () {
    function DemoUtilsModule() {
    }
    DemoUtilsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], angular_calendar__WEBPACK_IMPORTED_MODULE_3__["CalendarModule"]],
            declarations: [_calendar_header_component__WEBPACK_IMPORTED_MODULE_4__["CalendarHeaderComponent"]],
            exports: [_calendar_header_component__WEBPACK_IMPORTED_MODULE_4__["CalendarHeaderComponent"]]
        })
    ], DemoUtilsModule);
    return DemoUtilsModule;
}());



/***/ }),

/***/ "./src/app/shared/layouts/auth-layout/auth-layout.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/shared/layouts/auth-layout/auth-layout.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/shared/layouts/auth-layout/auth-layout.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/shared/layouts/auth-layout/auth-layout.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"auth-block\">\r\n  <mat-card>\r\n    <div>\r\n      <router-outlet></router-outlet>\r\n    </div>\r\n  </mat-card>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/shared/layouts/auth-layout/auth-layout.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/shared/layouts/auth-layout/auth-layout.component.ts ***!
  \*********************************************************************/
/*! exports provided: AuthLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthLayoutComponent", function() { return AuthLayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AuthLayoutComponent = /** @class */ (function () {
    function AuthLayoutComponent() {
    }
    AuthLayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-auth-layout',
            template: __webpack_require__(/*! ./auth-layout.component.html */ "./src/app/shared/layouts/auth-layout/auth-layout.component.html"),
            styles: [__webpack_require__(/*! ./auth-layout.component.css */ "./src/app/shared/layouts/auth-layout/auth-layout.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AuthLayoutComponent);
    return AuthLayoutComponent;
}());



/***/ }),

/***/ "./src/app/shared/layouts/main-layout/main-layout.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/shared/layouts/main-layout/main-layout.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\r\n  position: fixed;\r\n  top: 57px;\r\n  bottom: 0;\r\n  left: 0;\r\n  right: 0;\r\n  height: 100%;\r\n}\r\n\r\n.example-button {\r\n  width: 100%;\r\n  height: 7%;\r\n}\r\n\r\n.example-button-last {\r\n  width: 100%;\r\n  height: 7%;\r\n  top: 72%;\r\n}\r\n\r\n.example-sidenav {\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: center;\r\n  width: 200px;\r\n  /*background: rgba(255, 0, 0, 0.5);*/\r\n}\r\n\r\n.example-header {\r\n  position: fixed;\r\n  top: 0;\r\n  left: 0;\r\n  right: 0;\r\n}\r\n"

/***/ }),

/***/ "./src/app/shared/layouts/main-layout/main-layout.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/shared/layouts/main-layout/main-layout.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar class=\"example-header\">\r\n  <button mat-icon-button\r\n          *ngIf=\"screenWidth <= 992\"\r\n          (click)=\"sidenav.toggle()\">\r\n    <i class=\"material-icons\">menu</i>\r\n  </button>\r\n\r\n  <p class=\"mat-display-1 next-main-title\">What's Next?</p>\r\n</mat-toolbar>\r\n<mat-sidenav-container class=\"example-container\">\r\n  <mat-sidenav #sidenav\r\n               [opened]=\"screenWidth > 992\"\r\n               [mode]=\"screenWidth <= 992 ? 'over' : 'side'\"\r\n               class=\"example-sidenav\"\r\n               [fixedInViewport]=\"true\"\r\n               [fixedTopGap]=\"57\">\r\n\r\n    <button mat-button\r\n            [routerLink]=\"link.url\"\r\n            *ngFor=\"let link of links\"\r\n            [routerLinkActive]=\"'active'\"\r\n            (click)=\"closeBar()\"\r\n            class=\"example-button\">{{link.name}}\r\n    </button>\r\n\r\n    <button mat-button\r\n            class=\"example-button-last\"\r\n            (click)=\"logout($event)\">Logout\r\n    </button>\r\n\r\n  </mat-sidenav>\r\n\r\n  <mat-sidenav-content>\r\n    <router-outlet></router-outlet>\r\n  </mat-sidenav-content>\r\n\r\n</mat-sidenav-container>\r\n"

/***/ }),

/***/ "./src/app/shared/layouts/main-layout/main-layout.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/shared/layouts/main-layout/main-layout.component.ts ***!
  \*********************************************************************/
/*! exports provided: MainLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainLayoutComponent", function() { return MainLayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/shared/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MainLayoutComponent = /** @class */ (function () {
    function MainLayoutComponent(auth, router) {
        this.auth = auth;
        this.router = router;
        this.links = [
            { url: '/today', name: 'Today' },
            { url: '/tasks', name: 'Tasks' },
            { url: '/schedule', name: 'Schedule' }
        ];
    }
    MainLayoutComponent.prototype.ngOnInit = function () {
        this.screenWidth = window.innerWidth;
    };
    MainLayoutComponent.prototype.onResize = function (event) {
        this.screenWidth = window.innerWidth;
    };
    MainLayoutComponent.prototype.closeBar = function () {
        if (this.screenWidth <= 992) {
            this.sidenav.close();
        }
    };
    MainLayoutComponent.prototype.logout = function (event) {
        event.preventDefault();
        this.auth.logout();
        this.router.navigate(['/login']);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('sidenav'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSidenav"])
    ], MainLayoutComponent.prototype, "sidenav", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:resize', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], MainLayoutComponent.prototype, "onResize", null);
    MainLayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-main-layout',
            template: __webpack_require__(/*! ./main-layout.component.html */ "./src/app/shared/layouts/main-layout/main-layout.component.html"),
            styles: [__webpack_require__(/*! ./main-layout.component.css */ "./src/app/shared/layouts/main-layout/main-layout.component.css")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], MainLayoutComponent);
    return MainLayoutComponent;
}());



/***/ }),

/***/ "./src/app/shared/services/auth.service.ts":
/*!*************************************************!*\
  !*** ./src/app/shared/services/auth.service.ts ***!
  \*************************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jwt-decode */ "./node_modules/jwt-decode/lib/index.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jwt_decode__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthService = /** @class */ (function () {
    function AuthService(http) {
        this.http = http;
        this.token = null;
    }
    AuthService.prototype.login = function (user) {
        var _this = this;
        return this.http.post('/api/auth/login', user)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (_a) {
            var token = _a.token;
            localStorage.setItem('token', token);
            _this.setToken(token);
        }));
    };
    AuthService.prototype.registration = function (user) {
        return this.http.post('/api/auth/registration', user);
    };
    AuthService.prototype.setToken = function (token) {
        this.token = token;
    };
    AuthService.prototype.getToken = function () {
        return this.token;
    };
    AuthService.prototype.isAuthenticated = function () {
        var current_time = Date.now() / 1000;
        var tokenInfo;
        if (this.token) {
            tokenInfo = jwt_decode__WEBPACK_IMPORTED_MODULE_3__(this.token);
        }
        return this.token && tokenInfo && tokenInfo.exp && tokenInfo.exp > current_time;
    };
    AuthService.prototype.logout = function () {
        this.setToken(null);
        localStorage.removeItem('token');
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/shared/services/task.service.ts":
/*!*************************************************!*\
  !*** ./src/app/shared/services/task.service.ts ***!
  \*************************************************/
/*! exports provided: TaskService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskService", function() { return TaskService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TaskService = /** @class */ (function () {
    function TaskService(http) {
        this.http = http;
    }
    TaskService.prototype.gatAll = function () {
        return this.http.get('/api/task');
    };
    TaskService.prototype.create = function (task) {
        return this.http.post('/api/task', task);
    };
    // getById(id: string): Observable<Task> {
    //   return this.http.get<Task>(`/api/task/${id}`);
    // }
    TaskService.prototype.getByDay = function (date) {
        return this.http.get("/api/task/day/" + date);
    };
    // getByWeek(date: Date): Observable<Task> {
    //   return this.http.get<Task>(`/api/task/week/${date}`);
    // }
    TaskService.prototype.update = function (task) {
        return this.http.patch("/api/task/" + task._id, task);
    };
    TaskService.prototype.delete = function (id) {
        return this.http.delete("/api/task/" + id);
    };
    TaskService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], TaskService);
    return TaskService;
}());



/***/ }),

/***/ "./src/app/tasks-page/add-task-page/add-task-page.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/tasks-page/add-task-page/add-task-page.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/tasks-page/add-task-page/add-task-page.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/tasks-page/add-task-page/add-task-page.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form\r\n  [formGroup]=\"form\"\r\n  (ngSubmit)=\"onSubmit()\">\r\n  <mat-card-header>\r\n    <mat-form-field>\r\n      <input matInput\r\n             formControlName=\"taskName\"\r\n             placeholder=\"Task Name\"/>\r\n    </mat-form-field>\r\n    <!--TODO: display: table???-->\r\n\r\n  </mat-card-header>\r\n\r\n  <mat-card-content>\r\n    <div class=\"yk-g-12 yk-md-4\">\r\n      <owl-form-field>\r\n        <input owlInput\r\n               formControlName=\"date\"\r\n               [owlDateTime]=\"dt1\"\r\n               [selectMode]=\"'range'\"\r\n               [owlDateTimeTrigger]=\"dt1\"\r\n               placeholder=\"Date Time\">\r\n        <span owlSuffix><owl-date-time #dt1></owl-date-time></span>\r\n      </owl-form-field>\r\n    </div>\r\n    <mat-form-field style=\"width: 100%\">\r\n      <textarea matInput\r\n                formControlName=\"description\"\r\n                placeholder=\"Description\">\r\n      </textarea>\r\n    </mat-form-field>\r\n  </mat-card-content>\r\n  <mat-card-actions>\r\n    <button mat-flat-button\r\n            type=\"submit\"\r\n            color=\"primary\">\r\n      Save\r\n    </button>\r\n    <button mat-flat-button (click)=\"task.opened = !task.opened\">Cancel</button>\r\n  </mat-card-actions>\r\n</form>\r\n"

/***/ }),

/***/ "./src/app/tasks-page/add-task-page/add-task-page.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/tasks-page/add-task-page/add-task-page.component.ts ***!
  \*********************************************************************/
/*! exports provided: AddTaskPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddTaskPageComponent", function() { return AddTaskPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_services_task_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shared/services/task.service */ "./src/app/shared/services/task.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AddTaskPageComponent = /** @class */ (function () {
    function AddTaskPageComponent(taskService) {
        this.taskService = taskService;
    }
    AddTaskPageComponent.prototype.ngOnInit = function () {
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            taskName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.task.name, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.task.description, []),
            date: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]([this.task.startDate, this.task.dueDate], [])
        });
    };
    AddTaskPageComponent.prototype.onSubmit = function () {
        this.form.disable();
        this.task.name = this.form.get('taskName').value;
        this.task.description = this.form.get('description').value;
        this.task.startDate = this.form.get('date').value[0];
        this.task.dueDate = this.form.get('date').value[1];
        this.taskService.update(this.task)
            .subscribe(function (task) { return console.log('Saved task: ', task); });
        this.task.opened = !this.task.opened;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AddTaskPageComponent.prototype, "task", void 0);
    AddTaskPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-task-page',
            template: __webpack_require__(/*! ./add-task-page.component.html */ "./src/app/tasks-page/add-task-page/add-task-page.component.html"),
            styles: [__webpack_require__(/*! ./add-task-page.component.css */ "./src/app/tasks-page/add-task-page/add-task-page.component.css")]
        }),
        __metadata("design:paramtypes", [_shared_services_task_service__WEBPACK_IMPORTED_MODULE_2__["TaskService"]])
    ], AddTaskPageComponent);
    return AddTaskPageComponent;
}());



/***/ }),

/***/ "./src/app/tasks-page/tasks-page.component.css":
/*!*****************************************************!*\
  !*** ./src/app/tasks-page/tasks-page.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/tasks-page/tasks-page.component.html":
/*!******************************************************!*\
  !*** ./src/app/tasks-page/tasks-page.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"mat-display-1 page-title\">\r\n  <h4>Tasks</h4>\r\n</div>\r\n\r\n<button mat-raised-button\r\n        (click)=\"createNewTask()\"\r\n        class=\"next-create-button\"\r\n        color=\"primary\">Create\r\n</button>\r\n\r\n<mat-card class=\"card__task\"\r\n          *ngIf=\"createNewTaskTamplate\">\r\n\r\n  <form\r\n    [formGroup]=\"form\"\r\n    (ngSubmit)=\"onCreateNewTask()\">\r\n    <mat-card-header>\r\n      <mat-form-field>\r\n        <input matInput\r\n               formControlName=\"name\"\r\n               placeholder=\"Task Name\"/>\r\n      </mat-form-field>\r\n    </mat-card-header>\r\n\r\n    <mat-card-content>\r\n\r\n      <div class=\"yk-g-12 yk-md-4\">\r\n        <owl-form-field>\r\n          <input owlInput\r\n                 formControlName=\"date\"\r\n                 [owlDateTime]=\"dt1\"\r\n                 [selectMode]=\"'range'\"\r\n                 [owlDateTimeTrigger]=\"dt1\"\r\n                 placeholder=\"Date Time\">\r\n          <span owlSuffix><owl-date-time #dt1></owl-date-time></span>\r\n        </owl-form-field>\r\n      </div>\r\n\r\n      <!--<mat-form-field style=\"width: 100%\">\r\n        <label>\r\n          <input matInput\r\n                 formControlName=\"dueDate\"\r\n                 [matDatepicker]=\"myDatepicker\"\r\n                 placeholder=\"Due Date\">\r\n        </label>\r\n        <mat-datepicker-toggle matSuffix [for]=\"myDatepicker\"></mat-datepicker-toggle>\r\n        <mat-datepicker touchUi #myDatepicker></mat-datepicker>\r\n      </mat-form-field>-->\r\n\r\n      <mat-form-field style=\"width: 100%\">\r\n      <textarea matInput\r\n                formControlName=\"description\"\r\n                placeholder=\"Description\">\r\n      </textarea>\r\n      </mat-form-field>\r\n\r\n    </mat-card-content>\r\n    <mat-card-actions>\r\n      <button mat-flat-button\r\n              type=\"submit\"\r\n              color=\"primary\"\r\n              [disabled]=\"form.invalid || form.disabled\">\r\n        Create\r\n      </button>\r\n      <button mat-flat-button (click)=\"createNewTaskTamplate = false\">Cancel</button>\r\n    </mat-card-actions>\r\n  </form>\r\n</mat-card>\r\n\r\n<div *ngIf=\"tasks$ | async as tasks; else loader\">\r\n  <div\r\n    *ngIf=\"tasks.length !== 0; else empty\">\r\n    <mat-card class=\"card__task\"\r\n              *ngFor=\"let task of filterTasks(tasks)\">\r\n      <mat-checkbox [checked]=\"task.complete\"\r\n                    (change)=\"checkedTask(task)\">\r\n      </mat-checkbox>\r\n      <mat-card-header *ngIf=\"!task.opened\" class=\"next-task-card-header\">\r\n        <mat-card-title (click)=\"task.opened = !task.opened\">\r\n          №{{task.order}} - {{task.name}}\r\n        </mat-card-title>\r\n\r\n        <button mat-icon-button\r\n                (click)=\"deleteTask(task)\">\r\n          <i class=\"material-icons\">delete</i>\r\n        </button>\r\n\r\n      </mat-card-header>\r\n\r\n      <app-add-task-page *ngIf=\"task.opened\"\r\n                         style=\"width: 100%\"\r\n                         [task]=\"task\">\r\n      </app-add-task-page>\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<!--LOADER-->\r\n<ng-template #loader>\r\n  <app-loader></app-loader>\r\n</ng-template>\r\n\r\n<!--EMPTY-->\r\n<ng-template #empty>\r\n  <div>\r\n    You do not have any tasks yet\r\n  </div>\r\n</ng-template>\r\n"

/***/ }),

/***/ "./src/app/tasks-page/tasks-page.component.ts":
/*!****************************************************!*\
  !*** ./src/app/tasks-page/tasks-page.component.ts ***!
  \****************************************************/
/*! exports provided: TasksPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TasksPageComponent", function() { return TasksPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_task_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../shared/services/task.service */ "./src/app/shared/services/task.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var TasksPageComponent = /** @class */ (function () {
    function TasksPageComponent(taskService, snackBar, route) {
        this.taskService = taskService;
        this.snackBar = snackBar;
        this.route = route;
        this.opened = false;
        this.createNewTaskTamplate = false;
    }
    TasksPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getAllTasks();
        this.route.params
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["switchMap"])(function (params) {
            if (params['id']) {
                _this.tasks$ = _this.tasks$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (tasks) {
                    tasks.find(function (task) { return task._id === params['id']; }).opened = true;
                }));
            }
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["of"])(null);
        }))
            .subscribe(function () {
        }, function (error) {
        });
    };
    TasksPageComponent.prototype.filterTasks = function (tasks) {
        return tasks.filter(function (task) { return !task.complete; });
    };
    TasksPageComponent.prototype.checkedTask = function (task) {
        var _this = this;
        var trigger = true;
        var snackBarRef = this.snackBar.open('Task completed.', 'Return', { duration: 3000 });
        task.complete = true;
        snackBarRef.onAction().subscribe(function () {
            trigger = false;
            task.complete = false;
        });
        snackBarRef.afterDismissed().subscribe(function () {
            if (trigger) {
                _this.taskService.update(task).subscribe(function (task) {
                    console.log('Updated task: ', task);
                });
            }
        });
    };
    TasksPageComponent.prototype.deleteTask = function (task) {
        var _this = this;
        var trigger = true;
        var snackBarRef = this.snackBar.open('Task deleted.', 'Return', { duration: 3000 });
        task.complete = true;
        snackBarRef.onAction().subscribe(function () {
            trigger = false;
            task.complete = false;
        });
        snackBarRef.afterDismissed().subscribe(function () {
            if (trigger) {
                _this.taskService.delete(task._id).subscribe(function (task) {
                    console.log('Deleted task: ', task);
                });
            }
        });
    };
    TasksPageComponent.prototype.createNewTask = function () {
        this.createNewTaskTamplate = true;
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', []),
            date: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [])
        });
    };
    TasksPageComponent.prototype.onCreateNewTask = function () {
        var _this = this;
        this.form.disable();
        var newTask = {
            name: this.form.get('name').value,
            description: this.form.get('description').value,
            startDate: this.form.get('date').value[0],
            dueDate: this.form.get('date').value[1]
        };
        this.taskService.create(newTask)
            .subscribe(function (task) {
            console.log('Created task: ', task);
            _this.getAllTasks();
        });
        this.createNewTaskTamplate = false;
    };
    TasksPageComponent.prototype.getAllTasks = function () {
        this.tasks$ = this.taskService.gatAll()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (tasks) {
            tasks.forEach(function (tasks) {
                tasks.opened = false;
            });
        }));
        this.screenWidth = window.innerWidth;
    };
    TasksPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-tasks-page',
            template: __webpack_require__(/*! ./tasks-page.component.html */ "./src/app/tasks-page/tasks-page.component.html"),
            styles: [__webpack_require__(/*! ./tasks-page.component.css */ "./src/app/tasks-page/tasks-page.component.css")]
        }),
        __metadata("design:paramtypes", [_shared_services_task_service__WEBPACK_IMPORTED_MODULE_1__["TaskService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]])
    ], TasksPageComponent);
    return TasksPageComponent;
}());



/***/ }),

/***/ "./src/app/today-page/today-page.component.html":
/*!******************************************************!*\
  !*** ./src/app/today-page/today-page.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"tasks$ | async; else loader\">\r\n  <div class=\"col-md-4\" >\r\n    <h3 class=\"h3-title\">{{ viewDate | calendarDate:(view + 'ViewTitle'):'en' }}</h3>\r\n  </div>\r\n  <mat-card\r\n            style=\"margin-bottom: 20px;\">\r\n    <mwl-calendar-day-view\r\n      [viewDate]=\"viewDate\"\r\n      [events]=\"events\"\r\n      (eventClicked)=\"eventClicked($event)\">\r\n    </mwl-calendar-day-view>\r\n  </mat-card>\r\n</div>\r\n\r\n<!--LOADER-->\r\n<ng-template #loader>\r\n  <app-loader></app-loader>\r\n</ng-template>\r\n"

/***/ }),

/***/ "./src/app/today-page/today-page.component.ts":
/*!****************************************************!*\
  !*** ./src/app/today-page/today-page.component.ts ***!
  \****************************************************/
/*! exports provided: TodayPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TodayPageComponent", function() { return TodayPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angular_calendar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angular-calendar */ "./node_modules/angular-calendar/fesm5/angular-calendar.js");
/* harmony import */ var _shared_services_task_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/services/task.service */ "./src/app/shared/services/task.service.ts");
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! date-fns */ "./node_modules/date-fns/index.js");
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(date_fns__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TodayPageComponent = /** @class */ (function () {
    function TodayPageComponent(taskService, router) {
        this.taskService = taskService;
        this.router = router;
        this.viewDate = new Date();
        this.view = angular_calendar__WEBPACK_IMPORTED_MODULE_1__["CalendarView"].Day;
        this.events = [];
    }
    TodayPageComponent.prototype.ngOnInit = function () {
        this.getTasks();
    };
    TodayPageComponent.prototype.eventClicked = function (_a) {
        var event = _a.event;
        this.router.navigate(['/tasks/' + event.id]);
    };
    TodayPageComponent.prototype.getTasks = function () {
        var _this = this;
        this.tasks$ = this.taskService.getByDay(this.viewDate).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (tasks) {
            tasks.forEach(function (task) {
                _this.events.push({
                    id: task._id,
                    start: Object(date_fns__WEBPACK_IMPORTED_MODULE_3__["addHours"])(new Date(task.startDate), 0),
                    end: Object(date_fns__WEBPACK_IMPORTED_MODULE_3__["addHours"])(new Date(task.dueDate), 0),
                    title: task.name
                });
            });
        }));
    };
    TodayPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-today-page',
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
            template: __webpack_require__(/*! ./today-page.component.html */ "./src/app/today-page/today-page.component.html")
        }),
        __metadata("design:paramtypes", [_shared_services_task_service__WEBPACK_IMPORTED_MODULE_2__["TaskService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], TodayPageComponent);
    return TodayPageComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var materialize_css_dist_js_materialize__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! materialize-css/dist/js/materialize */ "./node_modules/materialize-css/dist/js/materialize.js");
/* harmony import */ var materialize_css_dist_js_materialize__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(materialize_css_dist_js_materialize__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_5__);






if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\Projects\WebStormProjects\what_is_next\client\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map