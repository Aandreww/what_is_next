const express = require('express');
const controller = require('../controllers/task');
const passport = require('passport');
const router = express.Router();

router.post('/', passport.authenticate('jwt', {session: false}), controller.create);

router.get('/', passport.authenticate('jwt', {session: false}), controller.getAll);

router.get('/:id', passport.authenticate('jwt', {session: false}), controller.getById);

router.get('/day/:date', passport.authenticate('jwt', {session: false}), controller.getByDay);

router.get('/week/:date', passport.authenticate('jwt', {session: false}), controller.getByWeek);

router.patch('/:id', passport.authenticate('jwt', {session: false}), controller.update);

router.delete('/:id', passport.authenticate('jwt', {session: false}), controller.remove);

module.exports = router;
